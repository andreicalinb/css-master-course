# What

This is the support code for [this](https://www.udemy.com/course/css-master-course/) course.

# Why
The scope was to improve my CSS skill.

In addition, for each course project, I did 5 custom features for each project in order to exercise my CSS skill.